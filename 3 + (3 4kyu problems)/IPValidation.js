function isValidIP(str) {
  function is_numeric(strr){
    return /^\d+$/.test(strr);
 }
  var cnt = 0;
  for(var i = 0; i < str.length; i++) {
      if(str[i] == '.') cnt++;
      if(!is_numeric(str[i]) && str[i] != '.') return false;
  }
  if(cnt != 3) return false;
  for(var i = 0; i < str.length; i++) {
    var cur = "";
    for(var j = i; j < str.length; j++) {
      if(str[j] == '.') break;
      cur += str[j];
    }
    i = j;
    if(cur[0] == '0' && cur.length > 1) return false;
    var numb = parseInt(cur, 10);
    if(numb > 255 || numb < 0) return false;
  }
  return true;
}
