function highAndLow(numbers) {
  var myArray = numbers.split(" ");
  for(var i=0; i<myArray.length; i++) { myArray[i] = parseInt(myArray[i], 10); }
  var mx = -Math.pow(2, 32), mn = Math.pow(2, 32);
  for(var i = 0; i < myArray.length; i++) {
    if(myArray[i] > mx) {
      mx = myArray[i];
    }
    if(myArray[i] < mn) {
      mn = myArray[i];
    }
  }
  var res = mx + " " + mn;
  return res;
}
