function solution(roman) {
    var result = 0, numbers = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1], characters = ["M", "CM","D","CD","C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
    for (var i = 0; i <= numbers.length; i++) {
        while(roman.indexOf(characters[i]) === 0){
            result += numbers[i];
            roman = roman.replace(characters[i], '');
        }
    }
    return result;
}
