function nextSmaller(n) {
    var number = n.toString();
    var checker = function(n) {
        for(var i = 0; i < n.length; i++) {
            for(var j = i + 1; j < n.length; j++) {
                if(i == 0) {
                    //without zero
                    if(parseInt(n[j], 10) < parseInt(n[i], 10) && parseInt(n[j], 10) != 0) {
                        return true;
                    }
                }
                else {
                    if(parseInt(n[j], 10) < parseInt(n[i], 10)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    if(checker(number) == false) {
        return -1;
    }
    else {
        function setCharAt(str, index,chr) {
                if(index > str.length - 1) return str;
                return str.substr(0, index) + chr + str.substr(index + 1);
        }
        var index = 0;
        var mn = [];
        for(var i = number.length - 1; i >= 1; i--) {
            for(var j = i - 1; j >= 0; j--) {
                if(parseInt(number[j], 10) > parseInt(number[i], 10)) {
                    //swap i, j
                    var fake = number;
                    fake = setCharAt(fake, j, number[i]);
                    fake = setCharAt(fake, i, number[j]);
                    mn.push(parseInt(fake, 10));
                }
             }
        }
        mn.sort(function(a,b){return a - b})
        var original = number;
        number = mn[mn.length - 1].toString();
        var before = "";
        for(var i = 0; i < number.length; i++) {
            if(original[i] != number[i]) {
                index = i;
                break;
            }
        }
        for(var i = 0; i <= index; i++) {
            before += number[i];
        }
        //after
        var arr = [];
        for(var i = index + 1; i < number.length; i++) {
            arr.push(parseInt(number[i], 10));
        }
        arr.sort(function(a,b){return b - a})
        var after = "";
        for (var i = 0; i < arr.length; i++) {
            after += arr[i].toString();
        }
        var result = before + after;
        return parseInt(result, 10);
    }
}
