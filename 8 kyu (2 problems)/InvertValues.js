function invert(array) {
    var my = function(x) {
      if(parseInt(x, 10) < 0) return Math.abs(x);
      else return x - 2 * x;
    }
    var result = [];
    for(var i = 0; i < array.length; i++) {
      result.push(my(array[i]));
    }
    return result;
}
