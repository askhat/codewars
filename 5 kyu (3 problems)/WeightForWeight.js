function orderWeight(str) {
    if(str.length == 0) {
        return "";
    }
    var a = str.split(' ');
    var getProperty = function (propertyName) {
        return obj[propertyName];
    };
    var hash = function(n) {
        var cur = n.toString();
        var sum = 0;
        for (var i = 0; i < n.length; i++) {
            sum += parseInt(n[i], 10);
        }
        return sum;
    }
    var numbers = [];
    for (var i = 0; i < a.length; i++) {
        numbers.push({id: hash(a[i]), val: a[i]});
    }
    //if a - b negative, then a < b
    numbers.sort(function(a, b) {
        return parseInt(a.id, 10) - parseInt(b.id, 10);
    });
    var res = [];
    for (var i = 0; i < numbers.length; i++) {
        res.push(numbers[i].val);
    }
    var result = "";
    var i = 0;
    while(i < res.length) {
        var cur = hash(res[i]);
        var current = [];
        while(i < res.length && hash(res[i]) == cur) {
            current.push(parseInt(res[i], 10));
            i++;
        }
        current.sort(function(a, b) {
            return a.toString().localeCompare(b.toString());
        });
        var curString = "";
        for (var j = 0; j < current.length; j++) {
            curString += current[j].toString();
            curString += " ";
        }
        result += curString;
    }
    var toReturn = "";
    for (var i = 0; i < result.length - 1; i++) {
        toReturn += result[i];
    }
    return toReturn;
}
