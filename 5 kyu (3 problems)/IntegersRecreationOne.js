function listSquared(m, n) {
    var res = [];
    for(var i = m; i <= n; i++) {
       var div = [];
       for(var j = 1; j <= Math.sqrt(i); j++) {
          if(i % j == 0) {
            if(j * j != i) {
               div.push(j);
               div.push(i / j);
            }
            else {
              div.push(j);
            }
          }
       }
       var sum = 0;
       for(var j = 0; j < div.length; j++) {
         sum += div[j] * div[j];
       }
       var sq = parseInt(Math.sqrt(sum), 10);
       if(sq * sq == sum) {
         var pusher = [];
         pusher.push(i);
         pusher.push(sum);
         res.push(pusher);
       }
    }
    return res;
}
