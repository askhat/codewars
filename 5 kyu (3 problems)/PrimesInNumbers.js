function primeFactors(n) {
    var getPrimes = function(n) {
        var cur = n;
        var res = [];
        while(cur != 1) {
            for(var i = 2; i <= cur; i++) {
                if(cur % i == 0) {
                    res.push(i);
                    cur /= i;
                    break;
                }
            }
        }
        for (var i = 0; i < res.length; i++) {
            console.log(res[i]);
        }
        return res;
    }
    var i = 0;
    var res = getPrimes(n);
    var result = "";
    while(i < res.length) {
        var cur = res[i];
        var cnt = 0;
        while(i < res.length && res[i] == cur) {
            i++;
            cnt++;
        }
        if(cnt > 1) {
            result += "(" + cur.toString() + "**" + cnt.toString() + ")";
        }
        else {
            result += "(" + cur.toString() + ")";
        }
    }
    return result;
}
