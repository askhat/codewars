function dontGiveMeFive(start, end) {
  var res = end - start + 1;
  for(var i = start; i <= end; i++) {
    var cur = i.toString();
    for(var j = 0; j < cur.length; j++) {
      if(cur[j] == '5') {
        res--;
        break;
      }
    }
  }
  return res;
}
