function makeParts(arr, chunkSize) {
  var res = [], cnt = 0, cur = [];
  for(var i = 0; i < arr.length; i++) {
    if(cnt == chunkSize) {
        cnt = 0;
        res.push(cur);
        cur = [];
    }
    cur.push(arr[i]);
    cnt++;
  }
  res.push(cur);
  return res;
}
