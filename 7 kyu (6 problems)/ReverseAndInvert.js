function reverseInvert(a) {
    var reverse = function(n) {
        var res = "";
        for(var i = n.length - 1; i >= 0; i--) {
            res += n[i];
        }
        return parseInt(res, 10);
    }
    var result = [];
    for(var i = 0; i < a.length; i++) {
        if(Number.isInteger(a[i])) {
            if(a[i] < 0) result.push(reverse(a[i].toString().substr(1, a[i].toString().length - 1)));
            else result.push(parseInt("-" + reverse(a[i].toString()).toString(), 10));
        }
    }
    return result;
}
