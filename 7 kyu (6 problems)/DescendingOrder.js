function descendingOrder(n) {
  var numbers = [];
  var cur = n.toString();
  //console.log(cur);
  if(cur.length == 1) {
    return n;
  }
  for(var i = 0; i < cur.length; i++) {
    numbers.push(parseInt(cur[i], 10));
  }
  numbers.sort(function(a,b){return b - a})
  var res = "";
  for(var i = 0; i < numbers.length; i++) {
    res += numbers[i].toString();
  }
  var result = parseInt(res, 10);
  return result;
}
