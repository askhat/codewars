function backwardsPrime(m, n) {
    var  result = [];
    var isPrime = function(n) {
        for(var i = 2; i <= Math.sqrt(n); i++) {
            if(n % i == 0) return false;
        }
        return true;
    }
    var reverser = function(n) {
        var res = "";
        for(var i = n.length - 1; i >= 0; i--) {
            res += n[i];
        }
        return parseInt(res, 10);
    }
    var isPalindrome = function(n) {
        for(var i = 0; i < parseInt(n.length / 2, 10); i++) {
            if(n[i] != n[n.length - i - 1]) return false;
        }
        return true;
    }
    for(var i = m; i <= n; i++) {
        if(isPrime(i) && isPrime(reverser(i.toString())) && !isPalindrome(i.toString())) {
            result.push(i);
        }
    }
    return result;
}
