function onLine(points) {
  if (points.length < 3) return true;
  function col(p1, p2, p3) {
    return (p1[1] - p2[1]) * (p1[0] - p3[0]) ==  (p1[1] - p3[1]) * (p1[0] - p2[0]);
  }
  var result = col.apply(null, points.slice(i, 3));
  for(var i = 1; i < points.length - 2; i++) result = result && col.apply(null, points.slice(i, i+3));
  return result;
}
