function validPhoneNumber(number) {
  var n = 0, c = 0, dash = 0;
  for (var i = 0; i < number.length; i++) {
    if(number[i] >= '0' && number[i] <= '9') {
      n++;
    }
    else if(number[i] == ')' || number[i] == '(') {
      c++;
    }
    else if(number[i] == '-') {
      dash++;
    }
    else if(number[i] != ' ') {
      return false;
    }
  }
  if(c != 2 && n != 10 && dash != 1) return false;
  if(number[0] != '(' || number[4] != ')' || number[9] != '-') return false;
  return true;
}
