function findOdd(A) {
  for(var i = 0; i < A.length; i++) {
    var cur = A[i];
    var cnt = 0;
    for(var j = 0; j < A.length; j++) {
      if(A[j] == cur) {
        cnt++;
      }
    }
    if(cnt % 2 == 1) {
      return cur;
    }
  }
  return 0;
}
