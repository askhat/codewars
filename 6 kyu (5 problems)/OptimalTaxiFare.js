function calculateOptimalFare(D, T, V1, R, V2) {
  // TODO: return minimal taxi fare (e.g. "3.14") or "Won't make it!"
  var cur = D / Math.max(V1, V2);
  if(cur * 60 > T) {
    return "Won't make it!";
  }
  if(V1 == D) {
    return 1;
  }
  var curTime = D / V2;
  if(curTime * 60 <= T) {
    return 0;
  }
  var result = [];
  for(var i = 0.000; i <= D; i += 0.001) {
    var t1 = i / V1;
    var t2 = (D - i) / V2;
    if((t1 + t2) * 60 <= T) {
      var current = i * R;
      result.push(current);
    }
  }
  result.sort(function(a,b){return a - b})
  return result[0].toPrecision(8);
}
